<?php

namespace Drupal\nostr_simple_publish\Services;

/**
 * Interface for Nostr storage.
 */
interface NostrStorageInterface {

  /**
   * Stores a Nostr event ID for an entity.
   *
   * @param int $entity_id
   *   Entity id.
   * @param string $entity_type_id
   *   Entity type id.
   * @param string $event_id
   *   Event id.
   */
  public function storeEventId(int $entity_id, string $entity_type_id, string $event_id);

  /**
   * Get the Nostr Event ID for an entity.
   *
   * @param int $entity_id
   *   Entity id.
   * @param string $entity_type_id
   *   Entity type id.
   *
   * @return string|false
   *   Return a string of the event id or false.
   */
  public function getEventId(int $entity_id, string $entity_type_id): string|FALSE;

}
