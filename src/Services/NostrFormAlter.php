<?php

namespace Drupal\nostr_simple_publish\Services;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use swentel\nostr\Event\Event;
use swentel\nostr\Filter\Filter;
use swentel\nostr\Key\Key;
use swentel\nostr\Message\RequestMessage;
use swentel\nostr\Relay\Relay;
use swentel\nostr\Request\Request;
use swentel\nostr\Subscription\Subscription;

/**
 * NostrFormAlter service class.
 */
class NostrFormAlter implements NostrFormAlterInterface {

  use StringTranslationTrait;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected AccountInterface $currentUser;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected MessengerInterface $messenger;

  /**
   * Nostr storage service.
   *
   * @var \Drupal\nostr_simple_publish\Services\NostrStorageInterface
   */
  protected NostrStorageInterface $nostrStorage;

  /**
   * Nostr publish service.
   *
   * @var \Drupal\nostr_simple_publish\Services\NostrPublishInterface
   */
  protected NostrPublishInterface $nostrPublish;

  /**
   * NostrFormAlter constructor.
   *
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\nostr_simple_publish\Services\NostrStorageInterface $nostr_storage
   *   The nostr storage.
   * @param \Drupal\nostr_simple_publish\Services\NostrPublishInterface $nostr_publish
   *   The nostr publish message.
   */
  public function __construct(AccountInterface $current_user, MessengerInterface $messenger, NostrStorageInterface $nostr_storage, NostrPublishInterface $nostr_publish) {
    $this->currentUser = $current_user;
    $this->nostrStorage = $nostr_storage;
    $this->messenger = $messenger;
    $this->nostrPublish = $nostr_publish;
  }

  /**
   * {@inheritdoc}
   */
  public function addNostrFormElement(&$form, FormStateInterface $form_state) {
    /** @var \Drupal\Core\Entity\EntityInterface $entity */
    $entity = $form_state->getFormObject()->getEntity();

    if ($entity && $this->currentUser->hasPermission('publish to nostr network')) {
      $can_publish = FALSE;
      // Check if this entity has a valid field with contents which can be broadcasted as a note.
      $is_note = FALSE;
      $content_fields = Settings::get('nostr_content_fields', ['body']);
      foreach ($content_fields as $content_field) {
        if ($entity->hasField($content_field)) {
          if ($entity->getFieldDefinitions()[$content_field]->getType() === 'string_long') {
            $is_note = TRUE;
          }
        }
      }
      if ($entity->isNew() || !($event_id = $this->nostrStorage->getEventId($entity->id(), $entity->getEntityTypeId()))) {
        $can_publish = TRUE;
      }

      if (isset($form['nostr_wrapper']) === FALSE) {
        $form['nostr_wrapper'] = [
          '#type' => 'details',
          '#title' => $this->t('Nostr'),
          '#open' => TRUE,
          '#group' => 'advanced',
        ];
      }
      if(isset($form['nostr_wrapper']['nostr_info']) === TRUE) {
        unset($form['nostr_wrapper']['nostr_info']);
      }

      if (isset($event_id) && $event_id !== FALSE) {
        // TODO fetch event.
        $relays = Settings::get('nostr_relays');
        $relay = $relays['outbox'][0];
        $event = $this->requestNostrEvent($event_id, $relay);
        // TODO get bech32-encoded identifier.
        $authorOfEvent = $this->requestNostrProfile($event->pubkey, 'wss://purplepag.es');
        $key =  new Key();
        $authorNpub = $key->convertPublicKeyToBech32($authorOfEvent['pubkey']);

        $truncated_event_id = substr($event_id,0,6).'...'.substr($event_id, -4);
        $form['nostr_wrapper']['sent'] = [
          '#markup' => '<p>' . $this->t('This note has been broadcasted to the Nostr network by <a href="https://njump.me/@authorNpub" target="_blank">@nostrAuthor</a>.<br /><br />View: <a href="https://njump.me/@id" target="_blank">@id_truncated</a>',
              [
                '@nostrAuthor' => $authorOfEvent['name'],
                '@authorNpub' => $authorNpub,
                '@id' => $event_id,
                '@id_truncated' => $truncated_event_id,
              ]) . '</p>',
        ];
      }

      if ($can_publish === TRUE && $is_note === TRUE) {
        $form['nostr_wrapper']['nostr_publish_note'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('Broadcast note'),
          '#states' => [
            'checked' => [
              ':input[name="user_select"]' => ['filled' => TRUE],
            ],
          ],
        ];
        // Show select dropdown with users which don't have an empty field_nsec.
        $uids = $this->getUsersWithNsec();
        // Add select form element.
        $form['nostr_wrapper']['user_label'] = [
          '#type' => 'label',
          '#title' => 'Send as',
        ];
        $form['nostr_wrapper']['user_select'] = [
          '#type' => 'select',
          '#options' => [],
          '#states' => [
            'required' => [
              ':input[name="nostr_publish_note"]' => ['checked' => TRUE],
            ],
          ],
        ];
        /** @var array $users */
        $users = \Drupal::entityTypeManager()->getStorage('user')->loadMultiple($uids);
        $user_select_options = [];
        /** @var \Drupal\user\Entity\User $user */
        foreach ($users as $user) {
          // Extra check if user has field_nsec
          if ($user->hasField('field_nsec') === FALSE) {
            break;
          }
          $user_select_options[$user->id()] = $user->getAccountName();
        }
        $form['nostr_wrapper']['user_select']['#options'] = $user_select_options;

        foreach (array_keys($form['actions']) as $action) {
          if ($action === 'submit' && isset($form['actions'][$action]['#type']) && $form['actions'][$action]['#type'] === 'submit') {
            $form['actions'][$action]['#submit'][] = [
              $this,
              'submitNostrFormElement',
            ];
          }
        }
      } else if ($is_note === FALSE) {
        // TODO replace this hardcoded form element from nostr_content_nip23 module.
        if (array_key_exists('nostr_publish_nip23', $form['nostr_wrapper']) === FALSE) {
          $form['nostr_wrapper']['nostr_info'] = [
            '#prefix' => '<p>',
            '#suffix' => '</p>',
            '#markup' => 'This content can\'t be broadcasted.',
          ];
        }
      }
    }
  }

  /**
   * Submit function to send a post to the Nostr network.
   *
   * @param array $form
   *   Structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Current form state.
   */
  public function submitNostrFormElement(array $form, FormStateInterface $form_state) {
    if ($form_state->getValue('nostr_publish_note')) {

      // Get field_nsec value of selected user.
      if ($form_state->getValue('user_select')) {
        /** @var \Drupal\user\Entity\User $user */
        $user = \Drupal::entityTypeManager()->getStorage('user')->load($form_state->getValue('user_select'));
        if (!$user->hasField('field_nsec')) {
          $this->messenger->addWarning($this->t('This user his Nostr private key has not been found.'));
          return;
        }
        $private_key_file_content = $user->get('field_nsec')->getString();
      } else {
        $private_key_file = Settings::get('nostr_private_key_file');
        if (!file_exists($private_key_file)) {
          $this->messenger->addWarning($this->t('The file which contains your Nostr private key has not been found.'));
          return;
        }
        $private_key_file_content = trim(file_get_contents($private_key_file));
        if (empty($private_key_file_content)) {
          $this->messenger->addWarning($this->t('The Nostr private key file is empty.'));
          return;
        }
      }

      // Get public key from private key.
      $key = new Key();
      $private_key = $key->convertToHex($private_key_file_content);
      $public_key = $key->getPublicKey($private_key);

      if (empty($public_key)) {
        $this->messenger->addWarning($this->t('We could not get a public key.'));
        return;
      }

      // Get relays to broadcast to.
      $relays = Settings::get('nostr_relays');
      if (empty($relays)) {
        $this->messenger->addWarning($this->t('The nostr_relays setting is empty.'));
        return;
      }
      if (empty($relays['outbox'])) {
        $this->messenger->addWarning($this->t('The nostr_relays[\'outbox\'] key is empty and should contain an array with relay URLs.'));
        return;
      }

      // All good, let's publish!
      /** @var \Drupal\Core\Entity\EntityInterface $entity */
      $entity = $form_state->getFormObject()->getEntity();
      $this->nostrPublish->sendMessage($entity, $public_key, $private_key, $relays, TRUE);
    }
  }

  /**
   * Get all user ids with a configured field_nsec.
   *
   * @return array
   * @throws Exception
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getUsersWithNsec(): array {
    $userStorage = \Drupal::entityTypeManager()->getStorage('user');
    $uids = $userStorage->getQuery()
      ->condition('status', 1)
      ->condition('field_nsec', NULL, 'IS NOT NULL')
      ->accessCheck(TRUE)
      ->execute();
    if (empty($uids)) {
      throw new \RuntimeException('No users with nsec found.');
    }
    return $uids;
  }

  /**
   * @param string $event_id
   * @param string $relayUri
   * @return Object|null
   */
  private function requestNostrEvent(string $event_id, string $relayUri): Object|null {
    // Fetch event.
    $subscription = new Subscription();
    $subscriptionId = $subscription->setId();
    $filter = new Filter();
    $filter->setIds([$event_id]);
    $filters = [$filter];
    $requestMessage = new RequestMessage($subscriptionId, $filters);
    $relay = new Relay($relayUri);
    $request = new Request($relay, $requestMessage);
    $response = $request->send();
    return $response[$relayUri][0]->event;
  }

  /**
   * @param string $pubkey
   * @param $relayUri
   * @return array|null
   */
  private function requestNostrProfile (string $pubkey, $relayUri): array|null {
    $subscription = new Subscription();
    $subscriptionId = $subscription->setId();
    $filter = new Filter();
    $filter->setAuthors([$pubkey]);
    $filter->setKinds([0]);
    $filter->setLimit(1);
    $filters = [$filter];
    $requestMessage = new RequestMessage($subscriptionId, $filters);
    $relay = new Relay($relayUri);
    $request = new Request($relay, $requestMessage);
    $response = $request->send();
    return json_decode($response[$relayUri][0]->event->content, TRUE);
  }
}
