<?php

namespace Drupal\nostr_simple_publish\Services;

use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Psr\Log\LoggerInterface;
use swentel\nostr\Event\Event;
use swentel\nostr\Message\EventMessage;
use swentel\nostr\Relay\Relay;
use swentel\nostr\Relay\RelaySet;
use swentel\nostr\Sign\Sign;

/**
 * Nostr publish event class.
 */
class NostrPublish implements NostrPublishInterface {

  use DependencySerializationTrait;
  use StringTranslationTrait;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected AccountInterface $currentUser;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected MessengerInterface $messenger;

  /**
   * Nostr storage service.
   *
   * @var \Drupal\nostr_simple_publish\Services\NostrStorageInterface
   */
  protected NostrStorageInterface $nostrStorage;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * Show Drupal message or not.
   *
   * @var bool
   */
  protected bool $showDrupalMessage = FALSE;

  /**
   * NostrPublish constructor.
   *
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   * @param \Drupal\nostr_simple_publish\Services\NostrStorageInterface $nostr_storage
   *   The nostr storage.
   */
  public function __construct(AccountInterface $current_user, MessengerInterface $messenger, LoggerInterface $logger, NostrStorageInterface $nostr_storage) {
    $this->currentUser = $current_user;
    $this->nostrStorage = $nostr_storage;
    $this->messenger = $messenger;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public function sendMessage(EntityInterface $entity, string $public_key, string $private_key, array $relays, bool $show_drupal_message) {
    $this->showDrupalMessage = $show_drupal_message;

    // Get content from a field.
    $content = NULL;
    $content_fields = Settings::get('nostr_content_fields', ['body']);
    foreach ($content_fields as $content_field) {
      if ($entity->hasField($content_field)) {
        $content = trim($entity->get($content_field)->value);
        break;
      }
    }

    if (!isset($content)) {
      $this->showDrupalMessage($this->t('No content found to send to the Nostr network.'), 'warning');
      $this->logger->warning('No content found in the fields to send to the Nostr network for @entity_type_id @entity_id', [
        '@entity_type_id' => $entity->getEntityTypeId(),
        '@entity_id' => $entity->id(),
      ]);
      return;
    }

    $event = new Event();
    $event->setKind(1);
    $event->setPublicKey($public_key);
    $event->setTags([
        ['p', $public_key ],
        [ 'client', (\Drupal::request()->getHost() !== 'localhost') ? \Drupal::request()->getHost() : 'Drupal']
    ]);
    $event->setContent($content);

    $relaySet = new RelaySet();
    foreach ($relays['outbox'] as $relayUrl) {
        $relay = new Relay($relayUrl);
        $relaySet->addRelay($relay);
        // Add relay hint to note.
        $event->addTag(['r', $relayUrl]);
    }
    $signer = new Sign();
    $signer->signEvent($event, $private_key);
    $eventMessage = new EventMessage($event);
    $relaySet->setMessage($eventMessage);

    if ($this->currentUser->hasPermission('view nostr debugging')) {
      $this->showDrupalMessage(print_r('Sending event to relays' . print_r($relaySet->getRelays(), 1), 1), 'warning');
      $this->showDrupalMessage($eventMessage->generate(), 'warning');
    }

    try {
      /** @var [] $response */
      $relayResponses = $relaySet->send();
      foreach ($relayResponses as $response) {
        // View debugging information.
        if ($this->currentUser->hasPermission('view nostr debugging')) {
          $this->showDrupalMessage(print_r($response, 1), 'warning');
        }

        if ($response->isSuccess() === FALSE) {
          $failure = $response->message();
          $this->showDrupalMessage($this->t('Failed posting to Nostr network: @fail', ['@fail' => $failure]), 'error');
          $this->logger->error('Failed posting to Nostr network: @message', ['@message' => $failure]);
        }
        else {
          // Store the id.
          if (Settings::get('nostr_sent_nids', TRUE)) {
            $this->nostrStorage->storeEventId($entity->id(), $entity->getEntityTypeId(), $event->getId());
          }
        }
      }
    }
    catch (\Exception $e) {
      $this->showDrupalMessage($this->t('Error posting to Nostr network: @message', ['@message' => $e->getMessage()]), 'error');
      $this->logger->error('Error posting to Nostr network: @message', ['@message' => $e->getMessage()]);
    }
  }

  /**
   * Helper function to show a Drupal message.
   *
   * @param string $message
   *   The message.
   * @param string $type
   *   Type of message.
   */
  protected function showDrupalMessage(string $message, string $type = 'default') {
    if ($this->showDrupalMessage) {
      switch ($type) {
        case 'error':
          $this->messenger->addError($message);
          break;

        case 'warning':
          $this->messenger->addWarning($message);
          break;

        default:
          $this->messenger->addMessage($message);
          break;
      }
    }
  }

}
