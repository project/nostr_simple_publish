# Nostr simple publish for Drupal

Very simple and opinionated module to publish content to the Nostr network.
More info about Nostr: https://github.com/nostr-protocol/nostr

Currently only works for a single Drupal user.

## Requirements

- PHP 8.1+ and the GMP extension - https://www.php.net/manual/en/ref.gmp.php

## How it works

Adds a 'Publish to Nostr' fieldset to node forms, protected by the
'publish to nostr network' permission. It currently assumes the content of
your post is stored in the 'body' field, but this can be overridden by a
setting.

Once the node has been sent, you can't publish it anymore. The event id is
stored in the nostr_simple_publish table.

There's an extra permission to view debug information after trying to post to
the network.

## Configuration

Posts to the network need to be signed, so we need the public and private key
of your Nostr account and one relay.

```
// Path to your Nostr private key file.
// The values of the keys are the hex encoded versions, not bech32 (which
// either start with npub or nsec).
// Use https://damus.io/key/ or https://github.com/rot13maxi/key-convertr
// to convert.
$settings['nostr_private_key_file'] = '/path/to/private-key-file';
```

```
// Nostr relay - find other relays at https://nostr.watch/relays/find.
// This relay seems to work fine. In case you are not connected with this one,
// add it to your relays so posts show up in your client as well.
$settings['nostr_relays'] = [ 'outbox' => [ 'wss://relay.damus.io'] ]
```

```
// Content fields - defaults to 'body'.
$settings['nostr_content_fields'] = ['field_description'];
```

## Roadmap

Generally, it's not the purpose of this module to become a relay or a fully
featured nostr client, hence the name.
